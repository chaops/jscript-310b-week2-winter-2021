console.log("script loaded!");

// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)
const myObj = {
  firstName: 'Sean',
  lastName: 'Chao',
  'favorite food': 'rice',
  bestFriend: {
      firstName: 'John',
      lastName: 'Doe',
      'favorite food': 'noodle'
  }
};

// 2. console.log best friend's firstName and your favorite food
console.log(`My friend ${myObj.bestFriend.firstName} doesn't like ${myObj['favorite food']}.`);

// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X
console.log('Creating board...');
const ttt = [[' ','O',' '],[' ','X','O'],['X',' ','X']];

console.log(ttt[0]);
console.log(ttt[1]);
console.log(ttt[2]);

// 4. After the array is created, 'O' claims the top right square.
// Update that value.
console.log('Adding next step...');
ttt[0][2] = 'O';

// 5. Log the grid to the console.
console.log(ttt[0]);
console.log(ttt[1]);
console.log(ttt[2]);

// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test
let myEmail = 'foo@bar.baz';
const regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;
console.log(`Is ${myEmail} a valid email adress? ${regex.test(myEmail)}`);
myEmail = 'brett@ mall';
console.log(`Is ${myEmail} a valid email adress? ${regex.test(myEmail)}`);

// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
const assignmentDate = '1/21/2019';

const stringToDate = new Date(assignmentDate);
console.log(`Converted a string: "${assignmentDate}", to a date object: "${stringToDate}"`);

// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.
const dueDate = new Date(stringToDate.setDate(stringToDate.getDate() + 7));
console.log(`Due date: ${dueDate}`);

// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

let monthName = months[dueDate.getMonth()];
let timeTag = `<time datetime="${dueDate.toISOString().split('T')[0]}">${monthName} ${dueDate.getDate()}, ${dueDate.getFullYear()}</time>`;

// 10. log this value using console.log
console.log(`HTML time tag: ${timeTag}`);